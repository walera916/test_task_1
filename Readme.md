## How to init:
#### 1)cd .docker
#### 2)run docker-compose up -d
#### 3)bin/console doctrine:database:create
#### 4)bin/console doc:mig:mig
#### 5)bin/console app:import-asteroids


## Task
````
Back-end Test (PHP + Symfony)

This test requires:
Working with PHP8.* and Symfony
Mysql Database
Use Symfony coding standards
Nginx

Required Functionality
Implement a call to NASA API to retrieve the list of Asteroids based on their closest approach date to Earth (Near-Earth Objects - NEOs):
Use api.nasa.gov
API-KEY: N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD
Documentation: https://api.nasa.gov/ and go to Browse APIs -> Asteroids - NeoWs
Limit the query to the last 3 days.
Persist the resulting list in your DB without duplicates.

Define the data model as follows:
date
reference (neo_reference_id)
name
speed (kilometers_per_hour)
is hazardous (is_potentially_hazardous_asteroid)

Create the following routes in your application:
GET /neo/hazardous
return all DB entries which contain potentially hazardous asteroids (with pagination)
response format: JSON
GET /neo/fastest?hazardous=(true|false)
calculate and return the model of the fastest asteroid
with a hazardous parameter, where true means is hazardous
default hazardous value is false
response format: JSON
GET /neo/best-month?hazardous=(true|false)
calculate and return a calendar month in the current year with most asteroids
with a hazardous parameter, where true means is hazardous
default hazardous value is false
response format: JSON

Bonus points:
Clean and straightforward code
Use restful API best practices
Create functional tests for API endpoints.
Use Docker

Good luck!
````