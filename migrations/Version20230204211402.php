<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230204211402 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS asteroid (
    id VARCHAR(255) NOT NULL,
    date DATE NOT NULL,
    reference VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    speed FLOAT NOT NULL,
    is_hazardous BOOLEAN DEFAULT FALSE,
    PRIMARY KEY (id),
    UNIQUE KEY asteroid_unique (name, reference, date)
) ENGINE=InnoDB CHARACTER SET utf8;
SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema): void
    {
        $sql = <<<SQL
DROP TABLE IF EXISTS asteroid;
SQL;

        $this->addSql($sql);
    }
}
