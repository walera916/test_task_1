<?php

namespace App\Command;

use App\Service\DataSynchronizer;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:import-asteroids')]
final class ImportDataCommand extends Command
{
    public function __construct(
        private readonly DataSynchronizer $synchronizer,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->synchronizer->sync();
        } catch (\Throwable $exception) {
            $output->writeln(\sprintf('Error occurred while sync data: %s', $exception->getMessage()));

            return self::FAILURE;
        }

        $output->writeln('Synchronized successfully');

        return self::SUCCESS;
    }
}
