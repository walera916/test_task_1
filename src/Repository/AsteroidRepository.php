<?php

namespace App\Repository;

use App\Entity\Asteroid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

class AsteroidRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Asteroid::class);
    }

    public function save(Asteroid $asteroid): void
    {
        $this->getEntityManager()->persist($asteroid);
        $this->getEntityManager()->flush();
    }

    public function getQuery(): Query
    {
        return $this->createQueryBuilder('e')->getQuery();
    }

    public function findFastestAsteroid(bool $isHazardous = false): Asteroid
    {
        $query = $this->createQueryBuilder('e')
            ->where('e.isHazardous = :isHazardous')
            ->orderBy('e.speed', 'DESC')
            ->setParameter('isHazardous', $isHazardous)
            ->setMaxResults(1);

        return $query->getQuery()->getSingleResult();
    }
}
