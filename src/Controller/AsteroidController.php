<?php

namespace App\Controller;

use App\Entity\Asteroid;
use App\Repository\AsteroidRepository;
use App\Service\MonthStatisticService;
use App\Service\Paginator\PaginateService;
use App\Service\TypeResolver;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class AsteroidController extends AbstractController
{
    use TypeResolver;

    public function __construct(
        private readonly AsteroidRepository $asteroidRepository,
        private readonly MonthStatisticService $monthStatisticService,
        private readonly PaginateService $paginateService,
    ) {
    }

    #[Route('/neo/hazardous', name: 'api_hazardous')]
    public function index(Request $request): JsonResponse
    {
        $paginatedData = $this->paginateService->paginate(
            $this->asteroidRepository->getQuery(),
            $request->query->get('page', 1),
        );

        $responseData = [];
        /** @var Asteroid $asteroid */
        foreach ($paginatedData->getData() as $asteroid) {
            $responseData[] = [
                'id' => $asteroid->getId(),
                'date' => $asteroid->getDate(),
                'reference' => $asteroid->getReference(),
                'name' => $asteroid->getName(),
                'speed' => $asteroid->getSpeed(),
                'is_hazardous' => $asteroid->isHazardous(),
            ];
        }

        return new JsonResponse([
            'data' => $responseData,
            'pagination' => [
                'page' => $paginatedData->getPage(),
                'total' => $paginatedData->getTotal(),
            ],
        ]);
    }

    #[Route('/neo/fastest', name: 'api_fastest_asteroids')]
    public function fastestAsteroid(Request $request): JsonResponse
    {
        /** @var Asteroid $asteroid */
        $asteroid = $this->asteroidRepository->findFastestAsteroid(
            $this->resolveBool($request->query->get('hazardous', false))
        );

        return new JsonResponse([
            'data' => [
                [
                    'id' => $asteroid->getId(),
                    'date' => $asteroid->getDate(),
                    'reference' => $asteroid->getReference(),
                    'name' => $asteroid->getName(),
                    'speed' => $asteroid->getSpeed(),
                    'is_hazardous' => $asteroid->isHazardous(),
                ],
            ],
        ]);
    }

    #[Route('/neo/best-month', name: 'api_month_statistics')]
    public function monthStatistics(Request $request): JsonResponse
    {
        return new JsonResponse([
            'data' => $this->monthStatisticService->calculate(
                $request->query->get('year', Carbon::now()->subYear()->year),
                $this->resolveBool($request->query->get('hazardous', false)),
            ),
        ]);
    }
}
