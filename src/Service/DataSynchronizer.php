<?php

namespace App\Service;

use App\Entity\Asteroid;
use App\Repository\AsteroidRepository;
use Carbon\Carbon;

final class DataSynchronizer
{
    public function __construct(
        private readonly NasaApiClient $apiClient,
        private AsteroidRepository $repository
    ) {
    }

    public function sync(): void
    {
        $response = $this->apiClient->pull(Carbon::now()->subMonths(3));

        if (!\array_key_exists('near_earth_objects', $response)) {
            throw new \DomainException('No data in the response');
        }

        foreach ($response['near_earth_objects'] as $asteroidsByDate) {
            foreach ($asteroidsByDate as $asteroidData) {
                try {
                    $asteroid = new Asteroid(
                        name: $asteroidData['name'],
                        reference: $asteroidData['neo_reference_id'],
                        speed: (float) $asteroidData['close_approach_data'][0]['relative_velocity']['kilometers_per_hour'],
                        isHazardous: $asteroidData['is_potentially_hazardous_asteroid'],
                        date: Carbon::createFromFormat('Y-m-d', $asteroidData['close_approach_data'][0]['close_approach_date'])->toDateTimeImmutable(),
                    );

                    $this->repository->save($asteroid);
                } catch (\Throwable $exception) {
                    // TODO handle it: add logger. OR use default values for missing values higher to skip this block
                }
            }
        }
    }
}
