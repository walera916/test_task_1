<?php

namespace App\Service;

trait TypeResolver
{
    private function resolveBool(string $value): bool
    {
        if ('false' === $value) {
            return false;
        }

        return true;
    }
}