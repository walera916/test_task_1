<?php

namespace App\Service;

use Doctrine\DBAL\Connection;

final class MonthStatisticService
{
    public const SQL = '
SELECT MONTH(date) as month, COUNT(1) AS count FROM asteroid
WHERE YEAR(date) = :year
AND is_hazardous = :hazardous
GROUP BY MONTH(date)
ORDER BY MONTH(date);
    ';

    public function __construct(
        private readonly Connection $connection
    ) {
    }

     public function calculate(int $year, bool $hazardous = false): array
     {
         $stmt = $this->connection->prepare(self::SQL);

         return $stmt->executeQuery([
             'year' => $year,
             'hazardous' => $hazardous,
         ])->fetchAllAssociative();
     }
}
