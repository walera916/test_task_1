<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

final class NasaApiClient
{
    private const API_URL = 'https://api.nasa.gov/neo/rest/v1/feed?start_date=%s&api_key=%s';

    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly string $apiKey
    ) {
    }

    public function pull(string $fromDate): array
    {
        try {
            return $this->httpClient->request(
                'GET',
                sprintf(self::API_URL, $fromDate, $this->apiKey)
            )->toArray();
        } catch (\Throwable) {
            return [];
        }
    }
}
