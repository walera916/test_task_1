<?php

namespace App\Service\Paginator;

use App\Service\Paginator\Dto\PaginatedData;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

final class PaginateService
{
    public function paginate(
        Query $query,
        int $currentPage = 1,
        int $pageSize = 20
    ): PaginatedData {
        $paginator = new Paginator($query);

        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($currentPage - 1))
            ->setMaxResults($pageSize);

        $totalItems = \count($paginator);

        return new PaginatedData(
            total: (int) \ceil($totalItems / $pageSize),
            page: $currentPage,
            data: $paginator->getQuery()->getResult(),
        );
    }
}
