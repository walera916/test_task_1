<?php

namespace App\Service\Paginator\Dto;

final class PaginatedData
{
    public function __construct(
        private readonly int $total,
        private readonly int $page,
        private readonly array $data,
    ) {
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
