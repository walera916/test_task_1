<?php

namespace App\Entity;

use App\Repository\AsteroidRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

#[ORM\Entity(repositoryClass: AsteroidRepository::class)]
#[ORM\Table(name: 'asteroid')]
class Asteroid
{
    #[ORM\Column(type: Types::STRING)]
    #[ORM\Id]
    private string $id;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeInterface $date;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $reference;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $name;

    #[ORM\Column(type: Types::FLOAT, nullable: false)]
    private float $speed;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $isHazardous;

    public function __construct(
        string $name,
        string $reference,
        float $speed,
        bool $isHazardous,
        ?\DateTimeInterface $date
    ) {
        $this->id = Uuid::uuid4();
        $this->name = $name;
        $this->reference = $reference;
        $this->speed = $speed;
        $this->isHazardous = $isHazardous;
        $this->date = $date;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpeed(): float
    {
        return $this->speed;
    }

    public function isHazardous(): bool
    {
        return $this->isHazardous;
    }
}
